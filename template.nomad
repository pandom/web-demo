job "tf-microbot" {
  region = "ap-southeast-2"
  datacenters = ["aws"]
  # Rolling updates
  #update {
  #  stagger = "10s"
  #  max_parallel = 1
  # }
  group "web" {
    # We want 9 web servers initially
    count = 5
    task "cicd" {
      driver = "docker"
      config {
        image = "registry.gitlab.com/pandom/web-demo:image_version"
	auth {
		username = "pandom"
		password = "place_password_here"
     	}
	port_map {
          http = 80
        }
      }
     service {
        name = "microbot"
        tags = ["urlprefix-/"]
        port = "http"
      #  check {
      #    type = "http"
      #    path = "/"
      #    interval = "10s"
      #    timeout = "2s"
      #  }
      }
      env {
        DEMO_NAME = "nomad-intro"
      }
      resources {
        cpu = 500
        memory = 256
        network {
          mbits = 100
          port "http" {}
        }
      }
    }
  }
}